<?php

use Illuminate\Support\Facades\Route;
use Pongsit\Ability\Http\Controllers\AbilityController;

Route::prefix('ability')->middleware(['userCan:manage_ability'])->group(function () {
	Route::get('/', [AbilityController::class, 'index'])->name('ability');
	Route::post('/store', [AbilityController::class, 'store'])->name('ability.store');
});