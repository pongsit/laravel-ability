<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAbilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('abilities', function (Blueprint $table) {
            $table->string('description')->nullable();
            $table->unique('slug');
            $table->integer('power')->default(50);
            $table->tinyInteger('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('abilities', function (Blueprint $table) {
            if (Schema::hasColumn('abilities','description')) {
                $table->dropColumn('description');
                $table->dropColumn('power');
                $table->dropColumn('status');
                $table->dropUnique('abilities_slug_unique');
            }
        });
    }
}
