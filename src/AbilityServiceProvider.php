<?php

namespace Pongsit\Ability;

use GuzzleHttp\Middleware;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Pongsit\Ability\Providers\EventServiceProvider;

class AbilityServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->mergeConfigFrom(
        //     __DIR__.'/../config/role.php', 'services'
        // );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['router']->namespace('Pongsit\\Ability\\Controllers')
                ->middleware(['web'])
                ->group(function () {
                    $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
                });
        // $this->app['router']->aliasMiddleware('isAdmin', IsAdmin::class);
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'ability');
        $this->publishes([
            __DIR__.'/../config/ability.php' => config_path('ability.php'),
        ], 'public');
    }
}