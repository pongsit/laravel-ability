<?php

namespace Pongsit\Ability\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Response;
use Pongsit\User\Models\User;
use Throwable;
use Image;
use Pongsit\Ability\Models\Ability;
use Pongsit\System\Models\System;

class AbilityController extends Controller
{
	public function index(){

        if(!user()->can('manage_ability')){
            return abort(403);
        }

		$variables['abilities'] = Ability::where('status',1)->orderBy('power','desc')->get();
		return view('ability::index',$variables);
	}

	public function store(Request $request)
    {

        $request->validate([
            'slug' => 'required',
            'name' => 'required',
        ],[
            'slug.required'=>'กรุณากรอกชื่อภาษาอังกฤษ',
            'name.required'=>'กรุณากรอกชื่อแสดง',
        ]);

        $infos = [];
        $infos['name'] = $request->name;
        $infos['slug'] = $request->slug;

        if(!empty($request->description)){
            $infos['description'] = $request->description;
        }

        if(!empty($request->power)){
            $infos['power'] = $request->power;
        }

        if(isset($request->status)){
            $infos['status'] = $request->status;
        }

        if(!empty($request->id)){
            $ability = Ability::find($request->id);
            $ability->update($infos);
        }else{
        	$ability = new ability($infos);
            $ability->save();
        }

        return back()->with(['success'=>'เรียบร้อย']);
    }
}
