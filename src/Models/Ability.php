<?php

namespace Pongsit\Ability\Models;

use Pongsit\Role\Database\Factories\RoleFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ability extends Model
{
  use HasFactory;
  
  // Disable Laravel's mass assignment protection
  protected $guarded = [];

  public function roles(){
      return $this->belongsToMany('Pongsit\Role\Models\Role')->withTimestamps();
  }
}
