@props(['ability'=>'','id'=>$id,'title'=>$title])

<div class="modal fade" id="{{$id}}" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$title}}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{route('ability.store')}}">
          @csrf
          <div class="d-flex mb-2">
            <div style="width:80px;">ชื่ออังกฤษ*:</div>
            <div class="flex-fill">
              <input type="text" name="slug" class="form-control" placeholder="เช่น student" value="{{$ability->slug ?? ''}}">
            </div>
          </div>
          <div class="d-flex mb-2">
            <div style="width:80px;">ชื่อแสดง*:</div>
            <div class="flex-fill">
              <input type="text" name="name" class="form-control" placeholder="เช่น นักเรียน" value="{{$ability->name ?? ''}}">
            </div>
          </div>
          <div class="d-flex mb-2">
            <div style="width:80px;">คำอธิบาย:</div>
            <div class="flex-fill">
              <textarea name="description" class="form-control">{{$ability->description ?? ''}}</textarea>
            </div>
          </div>
          <div class="d-flex mb-2">
            <div style="width:80px;">Power:</div>
            <div class="flex-fill">
              <input type="text" name="power" class="form-control" placeholder="ตัวเลขระหว่าง 1-100 เช่น 50" value="{{$ability->power ?? 50}}">
            </div>
          </div>
          <div class="d-flex mb-2">
            <div style="width:80px;">สถานะ:</div>
            <div class="flex-fill">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="1" 
                  @if(!empty($ability)) @if($ability->status) checked @endif @else checked @endif
                >
                <label class="form-check-label" for="inlineRadio1">ปกติ</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="0" 
                  @if(!empty($ability)) @empty($ability->status) checked @endempty @endif
                >
                <label class="form-check-label" for="inlineRadio2">ยกเลิก</label>
              </div>
            </div>
          </div>
          @if(!empty($ability->id))
            <input type="hidden" name="id" value="{{$ability->id}}">
          @endif
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>
        <button type="button" class="btn btn-primary {{$id}}-submit">ส่งข้อมูล</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(function () {
        $('body').on('click', '.{{$id}}-submit', function (e) {
            $('#{{$id}}').find('form').submit();
            $('#{{$id}}').modal('hide');
        });
    });
</script>