@if(user()->can('manage_ability'))
<div class="card shadow mb-3">
    <div class="card-header pt-3 pb-3 d-flex justify-content-between">
        <div><h6 class="m-0 font-weight-bold">ความสามารถ</h6></div>
        <div><a href="{{route('ability')}}" ><i class="fas fa-edit"></i> </a></div>
    </div>
    <div class="card-body">
        ความสามารถในการเข้าถึงเนื้อหาต่างๆ สามารถนิยามได้ที่นี่ครับ
    </div>
</div>
@endif