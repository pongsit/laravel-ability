@extends('layouts.main')

@section('content')
<x-ability::modal.ability 
    id="insertMember" 
    title="เพิ่มรายการ" 
/>

<div class="container">
    <div class="row justify-content-center">
        @if(user()->can('manage_ability'))
        <div class="col-12 col-md-6 col-lg-4 pb-3">
            <div class="card shadow">
                <div class="card-header pt-3 pb-3 d-flex justify-content-between">
                    <div><h6 class="m-0 font-weight-bold">ความสามารถ</h6></div>
                    <div>
                        <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#insertMember">
                            <i class="fas fa-plus-circle"></i> เพิ่ม
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    @php
                        $bg = '';
                        $bg_mem = $bg; 
                        $empty = true; 
                    @endphp
                    @foreach($abilities as $v)
                        <x-ability::modal.ability 
                            :ability="$v" 
                            id="ability{{$v->id}}" 
                            title="แก้ไขวิชา"
                        />
                        <div class="d-flex justify-content-between {{$bg}}">
                            <div>{{$v->name}}</div>
                            <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#ability{{$v->id}}">
                               <i class="fas fa-edit"></i>
                            </a>
                        </div>
                        @php 
                            if(!empty($bg)){
                                $bg = $bg_mem;
                            }else{
                                $bg = 'dark';
                            }
                            $empty = false; 
                        @endphp
                    @endforeach
                    @if($empty)
                        กรุณาเพิ่มความสามารถ
                    @endempty
                </div>
            </div>
        </div>
        {{-- @foreach($abilities as $v)
            <div class="col-12 col-md-6 col-lg-4 px-2 pb-3">
                <div class="card shadow">
                    <div class="card-header pt-3 pb-3 d-flex justify-content-between">
                        <div><h6 class="m-0 font-weight-bold">{{$v->name}}</h6></div>
                        <div class="text-right">
                            <x-ability::modal.ability 
                                :ability="$v" 
                                id="ability{{$v->id}}" 
                                title="แก้ไขวิชา"
                            />
                            <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#ability{{$v->id}}">
                                <span class="text-nowrap"><i class="fas fa-edit"></i> แก้ไข</span>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="d-flex" class="flex-wrap">
                            <div style="width:100px;">Power:</div>
                            <div>{{$v->power}}</div>
                        </div>
                        <div class="d-flex flex-wrap">
                            <div style="width:100px;">คำอธิบาย:</div>
                            <div>{{$v->description}}</div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach --}}
        @endif
    </div>
</div>
@stop